// Mock Database
let posts = [];

//Post ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

//ADD POST DATA
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	//prevents the default behavior of an event
	//to prevent the page from reloading(default behavior of submit)
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	count ++;
	console.log(posts);
	alert('Post succesfully added!');
	showPosts();
});


//RETRIEVE POST
const showPosts = () => {
	// variable that will contain all the posts
	let postEntries = "";

	//looping through array items
	posts.forEach((post) => {
		postEntries += ` 
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries;

};


//EDIT POST (edit button)
const editPost = (id) => {

	//The function uses the querySelector() method to get the element with the Id "#post-title-${id}" and "#post-body-${id}" and assigns its innerHTML property to the title variable with the same body
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

};


//UPDATE POST (Update Button)
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Post succesfully updated!")
		}
	}
});



//[ACTIVITY]
//DELETE POST (Delete Button)
const deletePost = (id) => {

	for(let i = 0; i < posts.length; i++){
		posts.splice(i, 1);

	}

	document.querySelector(`#post-${id}`).remove();
} 
